wldhx/gitlab-ci-react-native-android
===========================================

.. image:: https://gitlab.com/wldhx/gitlab-ci-react-native-android/badges/master/build.svg
    :target: https://gitlab.com/wldhx/gitlab-ci-react-native-android/commits/master
    :alt: Build status

Augments images generated with wldhx/gitlab-ci-android to allow building React Native applications for Android. Similarly, built images are stored in GitLab Container Registry.

Example ``.gitlab-ci.yml``
--------------------------

.. code:: yaml

    image: registry.gitlab.com/wldhx/gitlab-ci-react-native-android:master

    stages:
      - build

    before_script:
      - export GRADLE_USER_HOME=$(pwd)/.gradle
      - chmod +x ./android/gradlew

    cache:
      key: ${CI_PROJECT_ID}
      paths:
      - node_modules/
      - .gradle/

    build:
      stage: build
      script:
        - yarn install
        - cd android/
        - ./gradlew assembleDebug
      artifacts:
        paths:
          - android/app/build/outputs/apk/app-debug.apk
